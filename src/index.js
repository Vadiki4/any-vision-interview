const express = require('express');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();
const app = express();

app.use(cors()); // allow all sources
app.use(bodyParser.json());
app.use(
	cookieSession({
		maxAge: 30 * 24 * 60 * 60 * 1000, // month session
		keys: [process.env.COOKIE_KEY || 'abc'],
	}),
);

// categorized routes
require('./routes/appRoutes')(app);
require('./routes/streamRoutes')(app);

// Api start
const PORT = process.env.PORT || 5000;
app.listen(PORT);
console.log('NodeJS server is running!');
