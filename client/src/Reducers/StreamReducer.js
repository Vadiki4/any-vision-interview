import produce from 'immer';
import { SAVE_MY_STREAMS, SAVE_STREAM, LOG_OUT } from '../Sagas/actions';

const initialState = {
	myStreams: [],
	streamPort: null,
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case SAVE_MY_STREAMS: {
				const { wsPort, streams } = action.payload;

				// check if there is changes in streams before update
				for (const stream of streams) {
					if (draft.myStreams.findIndex(str => str._id === stream._id) <= 0) {
						draft.myStreams = streams;
						break;
					}
				}
				draft.streamPort = wsPort;
				return draft;
			}
			case SAVE_STREAM: {
				draft.myStreams.push(action.payload);
				return draft;
			}
			case LOG_OUT: {
				// reset store
				return initialState;
			}
			default:
				return draft;
		}
	});
