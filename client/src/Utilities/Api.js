import axios from 'axios';
const baseUrl = 'http://localhost:5000';

export default {
	app() {
		const url = baseUrl + '/app';
		return {
			login: data => {
				return axios.post(`${url}/login`, data);
			},
			register: data => {
				return axios.post(`${url}/register`, data);
			},
		};
	},
	stream() {
		const url = baseUrl + '/stream';
		return {
			saveSteam: data => {
				return axios.post(
					`${url}/saveStream`,
					{
						streamUrl: data.streamUrl,
						streamName: data.streamName,
					},
					{ headers: { authorization: data.user.token } } // providing jwt token
				);
			},
			getStreams: data => {
				return axios.post(`${url}/getStreams`, null, {
					headers: { authorization: data.user.token },
				});
			},
			playStream: data => {
				return axios.post(
					`${url}/playStream`,
					{ streamUrl: data.streamUrl },
					{ headers: { authorization: data.user.token } }
				);
			},
			stopStream: data => {
				return axios.post(`${url}/stopStream`, {}, { headers: { authorization: data.user.token } });
			},
		};
	},
};
