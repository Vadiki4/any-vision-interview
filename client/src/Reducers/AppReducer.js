import produce from 'immer';
import { SET_USER, LOG_OUT } from '../Sagas/actions';

const initialState = {
	appAccount: {
		name: null,
		email: null,
		token: null,
		userId: null,
	},
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case SET_USER: {
				const { token, body } = action.payload;
				draft.appAccount = { ...draft.appAccount, ...body, token };
				return draft;
			}
			case LOG_OUT: {
				// reset store
				return initialState;
			}
			default:
				return draft;
		}
	});
