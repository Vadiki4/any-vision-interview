const db = require('../db');
const actions = require('../db/actions');
const jwt = require('jsonwebtoken');
const Stream = require('node-rtsp-stream');
const streamers = {}; // list of streaming logged in users
let portAvailable = 9999; // web sockets ports pool

module.exports = app => {
	app.post('/stream/saveStream', async (req, res) => {
		const { streamUrl, streamName } = req.body;
		const token = req.headers.authorization;
		// JWT token verification -> user retrival
		jwt.verify(token, process.env.JWT_KEY, async (err, user) => {
			if (err) return res.status(401).send({ message: 'No token provided' });
			if (user) {
				// save data to database
				const result = await db.stream({
					type: actions.SAVE_STREAM,
					data: { streamUrl, streamName, id: user.id },
				});
				if (result) {
					// return to client
					res.json(result);
				} else res.json({ error: 3, message: 'No user found' });
			} else {
				return { error: 1, message: 'No user provided' };
			}
		});
	});
	app.post('/stream/getStreams', async (req, res) => {
		const token = req.headers.authorization;
		// JWT token verification -> user retrival
		jwt.verify(token, process.env.JWT_KEY, async (err, user) => {
			if (err) return res.status(401).send({ message: 'No token provided' });
			if (user) {
				if (!streamers[user.id]) {
					// create streamer profile for current user
					streamers[user.id] = { wsPort: portAvailable, stream: null };
					portAvailable -= 1;
				}
				// retrieve data from database
				const result = await db.stream({ type: actions.GET_MY_STREAMS, data: { id: user.id } });
				// return to client
				res.status(200).send({ wsPort: streamers[user.id].wsPort, streams: result });
			} else {
				return { error: 1, message: 'No user provided' };
			}
		});
	});
	app.post('/stream/stopStream', async (req, res) => {
		const token = req.headers.authorization;
		// JWT token verification -> user retrival
		jwt.verify(token, process.env.JWT_KEY, async (err, user) => {
			if (err) return res.status(401).send({ message: 'No token provided' });
			if (user) {
				if (streamers[user.id]) {
					// stop streaming for current user
					streamers[user.id].stream.stop();
					streamers[user.id].stream = null;
				}
				res.json({ message: 'stream stopped' });
			} else {
				return { error: 1, message: 'No user provided' };
			}
		});
	});
	app.post('/stream/playStream', async (req, res) => {
		const { streamUrl } = req.body;
		const token = req.headers.authorization;
		// JWT token verification -> user retrival
		jwt.verify(token, process.env.JWT_KEY, async (err, user) => {
			if (err) return res.status(401).send({ message: 'No token provided' });
			if (user) {
				// retrieve data from database
				const { streamUrl: streamReadyUrl, streamName } = await db.stream({
					type: actions.PLAY_STREAM,
					data: { streamUrl, id: user.id },
				});
				if (streamReadyUrl) {
					// stop current user stream if available
					if (streamers[user.id] && streamers[user.id].stream) {
						streamers[user.id].stream.stop();
						streamers[user.id].stream = null;
					}
					if (!streamers[user.id]) {
						// create streamer profile for current user
						streamers[user.id] = { wsPort: portAvailable, stream: null };
						portAvailable -= 1;
					}
					// create new player for current user with requested content
					streamers[user.id].stream = new Stream({
						name: streamName,
						streamUrl: streamReadyUrl,
						wsPort: streamers[user.id].wsPort,
						ffmpegOptions: {
							'-stats': '',
							'-r': 30,
						},
					});
				} else res.json({ error: 3, message: 'No user found' });
			} else {
				return { error: 1, message: 'No user provided' };
			}
		});
	});
};
