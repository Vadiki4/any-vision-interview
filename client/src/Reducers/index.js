import { combineReducers } from 'redux';
import App from './AppReducer';
import Streams from './StreamReducer';

const reducers = combineReducers({
	app: App,
	streams: Streams,
});

export default reducers;
