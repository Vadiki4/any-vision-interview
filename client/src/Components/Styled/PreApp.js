import styled from 'styled-components';

export const LoginLayout = styled.div`
	width: 100%;
	background: #457fca;
	background: -webkit-linear-gradient(to right, #5691c8, #457fca);
	background: linear-gradient(to right, #5691c8, #457fca);
	height: ${props => props.height}px;
	overflow: hidden;
	display: flex;
	align-items: center;
	justify-content: center;

	.content {
		width: 50%;
		min-height: 350px;
		background: #fff;
		border-radius: 12px;
		padding: 15px;
		color: #2d2d2d;
		box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2);
		display: flex;
		align-items: center;
		flex-flow: column nowrap;
		max-width: 600px;

		.logo {
			height: 75px;
			font-size: 21px;
			font-weight: 500;
			color: #7d7d7d;
			display: flex;
			flex-flow: column nowrap;
			margin-bottom: 20px;

			svg {
				font-size: 35px;
				color: #1890ff;
			}
		}

		.elements {
			width: 80%;
			margin-bottom: 10px;

			input {
				margin-bottom: 30px;
				font-weight: 500;
				font-size: 18px;
				line-height: 45px;
				height: 45px;
			}
		}

		.options {
			width: 80%;
			display: flex;
			align-items: center;
			justify-content: space-between;

			button {
				min-width: 175px;
				font-weight: 500;
				font-size: 18px;
				margin-bottom: 30px;
				height: 45px;
			}
		}
	}
`;

export const RegisterLayout = styled.div`
	width: 100%;
	background: #457fca;
	background: -webkit-linear-gradient(to right, #5691c8, #457fca);
	background: linear-gradient(to right, #5691c8, #457fca);
	height: ${props => props.height}px;
	overflow: hidden;
	display: flex;
	align-items: center;
	justify-content: center;

	.content {
		width: 50%;
		min-height: 350px;
		background: #fff;
		border-radius: 12px;
		padding: 15px;
		color: #2d2d2d;
		box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2);
		display: flex;
		align-items: center;
		flex-flow: column nowrap;
		max-width: 600px;

		.logo {
			height: 75px;
			font-size: 21px;
			font-weight: 500;
			color: #7d7d7d;
			display: flex;
			flex-flow: column nowrap;
			margin-bottom: 20px;

			svg {
				font-size: 35px;
				color: #1890ff;
			}
		}

		.elements {
			width: 80%;
			margin-bottom: 10px;

			input {
				margin-bottom: 30px;
				font-weight: 500;
				font-size: 18px;
				line-height: 45px;
				height: 45px;

				&::placeholder {
					text-transform: capitalize;
				}
			}
		}

		.options {
			width: 80%;
			display: flex;
			align-items: center;
			justify-content: space-between;

			button {
				min-width: 175px;
				font-weight: 500;
				font-size: 18px;
				margin-bottom: 30px;
				height: 45px;
			}
		}
	}
`;
