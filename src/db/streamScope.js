async function getMyStreams(db, data) {
	try {
		const { id } = data;
		if (id) {
			return await streamPromise(db, id);
		} else {
			return null;
		}
	} catch (err) {
		console.error(err);
	}
}

async function saveStream(db, data) {
	try {
		const { id, streamUrl, streamName } = data;
		if (id) {
			await db.collection('streams').insertOne({ id, streamUrl, streamName });
			return await streamPromise(db, id);
		} else {
			return null;
		}
	} catch (err) {
		console.error(err);
	}
}

async function playStream(db, data) {
	try {
		const { id, streamUrl } = data;
		if (id) {
			const result = await db.collection('streams').findOne({ id, streamUrl });
			return result;
		} else {
			return null;
		}
	} catch (err) {
		console.error(err);
	}
}

const streamPromise = (db, id) => {
	return new Promise((resolve, reject) => {
		db.collection('streams')
			.find({ id })
			.toArray(function(err, data) {
				err ? reject(err) : resolve(data);
			});
	});
};

exports.getMyStreams = getMyStreams;
exports.saveStream = saveStream;
exports.playStream = playStream;
