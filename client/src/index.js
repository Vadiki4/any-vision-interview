import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'ress';
import './index.scss';
import Router from './Router';
import store from './createStore';
import 'antd/dist/antd.css';
import ErrorBoundary from './Components/Common/ErrorBoundary'; // catch unexpected errors

ReactDOM.render(
	<Provider store={store}>
		<ErrorBoundary>
			<Router />
		</ErrorBoundary>
	</Provider>,
	document.getElementById('root')
);
