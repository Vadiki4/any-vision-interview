import styled from 'styled-components';

export const HomeLayout = styled.div`
	width: 100%;
	height: ${props => props.height}px;
	display: flex;
	align-items: center;
	justify-content: center;
	/* background: lightblue; */
	background: #457fca; /* fallback for old browsers */
	background: -webkit-linear-gradient(to right, #5691c8, #457fca); /* Chrome 10-25, Safari 5.1-6 */
	background: linear-gradient(
		to right,
		#5691c8,
		#457fca
	); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

	.content {
		width: 50%;
		min-height: 250px;
		background: #fff;
		border-radius: 12px;
		padding: 15px;
		position: relative;
		color: #2d2d2d;
		box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2);
		display: flex;
		flex-flow: column nowrap;
		align-items: center;
		max-width: 600px;

		.title {
			text-align: center;
			font-size: 24px;
			font-weight: 500;
			margin-bottom: 20px;
			color: #777777;
			display: flex;
			flex-flow: column nowrap;
			justify-content: center;

			h3 {
				color: #777777;
			}
		}

		.url {
			display: flex;
			justify-content: center;
			width: 80%;
			flex-flow: column nowrap;
		}

		.stream {
			margin-bottom: 30px;
			width: 80%;

			canvas {
				width: 100%;
				height: 100%;
			}
		}

		label {
			font-size: 16px;
			font-weight: 500;
			color: #777777;
		}

		input {
			margin-bottom: 30px;
			width: 100%;
			height: 45px;
			line-height: 45px;
			font-weight: 500;
			font-size: 18px;
		}

		.options {
			width: 80%;
			display: flex;
			align-items: center;
			justify-content: space-between;
			overflow: hidden;
		}

		button {
			min-width: 150px;
			font-size: 18px;
			font-weight: 500;
			margin-bottom: 30px;
			height: 45px;
		}

		button + button {
			margin-left: 20px;
		}
	}
`;

export const GridLayout = styled.div`
	width: 100%;
	height: ${props => props.height}px;
	display: flex;
	align-items: center;
	justify-content: center;
	background: #457fca;
	background: -webkit-linear-gradient(to right, #5691c8, #457fca);
	background: linear-gradient(to right, #5691c8, #457fca);

	.content {
		width: 90%;
		height: 90%;
		background: #fff;
		border-radius: 12px;
		padding: 15px;
		color: #2d2d2d;
		box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.2);
		overflow-y: auto;

		.title {
			text-align: center;
			font-size: 24px;
			font-weight: 500;
			margin-bottom: 20px;
			color: #777777;
			display: flex;
			flex-flow: column nowrap;
			justify-content: center;
			position: relative;

			h3 {
				color: #777777;
			}

			button {
				position: absolute;
			}
		}

		.url {
			display: flex;
			justify-content: center;
			width: 80%;
		}

		label {
			font-size: 16px;
			font-weight: 500;
			color: #777777;
		}

		input {
			margin-bottom: 30px;
			width: 100%;
			height: 45px;
			line-height: 45px;
			font-weight: 500;
			font-size: 18px;
		}

		.options {
			width: 80%;
			display: flex;
			align-items: center;
			justify-content: space-between;
			overflow: hidden;
		}

		button {
			min-width: 150px;
			font-size: 18px;
			font-weight: 500;
			margin-bottom: 30px;
			height: 45px;
		}

		button + button {
			margin-left: 20px;
		}
	}
`;
