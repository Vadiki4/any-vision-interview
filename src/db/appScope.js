const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');

async function logIn(db, data) {
	try {
		const { email, password } = data;
		const user = await db.collection('users').findOne({ email });
		if (user) {
			if (!bcrypt.compareSync(password, user.password))
				return { error: 3, message: 'Bad password' };
			const userPayload = {
				email: user.email,
				name: user.name,
			};
			const token = jwt.sign({ ...userPayload, id: user._id }, process.env.JWT_KEY, {
				expiresIn: 86400, // 24 hour
			});
			return { ...userPayload, token };
		} else {
			return { error: 3, message: 'No user found' };
		}
	} catch (err) {
		console.error(err);
	}
}

async function register(db, data) {
	try {
		const { email, password, name } = data;
		const hashPassword = bcrypt.hashSync(password, null, null);
		const user = {
			email,
			password: hashPassword,
			name,
		};
		const result = await db.collection('users').insertOne(user);
		if (result) {
			const { email, _id, name } = result.ops[0];
			const userDetails = { email, name };
			const token = jwt.sign({ ...userDetails, id: _id }, process.env.JWT_KEY, {
				expiresIn: 86400, // 24 hour
			});
			return { token, ...userDetails };
		} else {
			return { error: 3, message: 'Bad Login' };
		}
	} catch (err) {
		console.error(err);
	}
}

exports.logIn = logIn;
exports.register = register;
