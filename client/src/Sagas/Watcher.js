import { takeLatest } from 'redux-saga/effects';
import { logIn, register } from './UserSagas';
import { getMyStreams, playStream, saveStream, stopStream } from './StreamSagas';
import { LOG_IN, REGISTER, GET_MY_STREAMS, SAVE_STREAM, PLAY_STREAM, STOP_STREAM } from '../Sagas/actions';

// register saga listeners
export function* userWatcher() {
	yield takeLatest(LOG_IN, logIn);
	yield takeLatest(REGISTER, register);
}

export function* streamWatcher() {
	yield takeLatest(GET_MY_STREAMS, getMyStreams);
	yield takeLatest(SAVE_STREAM, saveStream);
	yield takeLatest(PLAY_STREAM, playStream);
	yield takeLatest(STOP_STREAM, stopStream);
}
