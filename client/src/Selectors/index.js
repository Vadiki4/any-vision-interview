// import { createSelector } from 'reselect';

// GETTERS
export const getCurrentUser = state => state.app.appAccount;
export const getCurrentUserName = state => state.app.appAccount.name;
export const getMyStreams = state => state.streams.myStreams;
export const getWsPort = state => state.streams.streamPort;

// SELECTORS
