import React, { useEffect, useRef, useCallback } from 'react';
import { LoginLayout } from '../Components/Styled/PreApp';
import { Button, Input, message, Icon } from 'antd';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { LOG_IN } from '../Sagas/actions';

const LoginPage = () => {
	const passElRef = useRef();
	const emailElRef = useRef();
	const emailRef = useRef();
	const passRef = useRef();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const history = useHistory();
	const dispatch = useDispatch();

	const handleLogin = useCallback(() => {
		// validate form fields
		if (!emailRef.current || emailRef.current.length < 10) {
			emailElRef.current.focus();
			message.warn('Please fill your email');
			return;
		}
		if (!passRef.current || passRef.current.length < 4) {
			passElRef.current.focus();
			message.warn('Please fill your password');
			return;
		}

		dispatch({ type: LOG_IN, payload: { email, password } });
	}, [dispatch, email, password]);

	useEffect(() => {
		const handleEnterPress = e => e.keyCode === 13 && handleLogin();

		document.addEventListener('keypress', handleEnterPress);

		return () => {
			document.removeEventListener('keypress', handleEnterPress);
		};
	}, [handleLogin]);

	const handleInputChange = ({ target }) => {
		const { name, value } = target;
		if (name === 'email') {
			emailRef.current = value;
			setEmail(value);
		} else if (name === 'password') {
			passRef.current = value;
			setPassword(value);
		}
	};

	return (
		<LoginLayout height={window.innerHeight}>
			<div className='content'>
				<div className='logo'>
					<Icon type='key' />
					<span>Login</span>
				</div>
				<div className='elements'>
					<div className='element'>
						<Input
							name='email'
							value={email}
							placeholder='Email'
							type='email'
							size='large'
							ref={emailElRef}
							maxLength={50}
							onChange={handleInputChange}
						/>
					</div>
					<div className='element'>
						<Input
							name='password'
							type='password'
							placeholder='Password'
							value={password}
							ref={passElRef}
							maxLength={50}
							size='large'
							onChange={handleInputChange}
						/>
					</div>
				</div>
				<div className='options'>
					<Button size='large' onClick={handleLogin} type='primary'>
						Login
					</Button>
					<Button size='large' onClick={() => history.push('/register')} type='ghost'>
						Register
					</Button>
				</div>
			</div>
		</LoginLayout>
	);
};

export default LoginPage;
