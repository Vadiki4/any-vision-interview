import { call, put } from 'redux-saga/effects';
import Api from '../Utilities/Api';
import { SAVE_MY_STREAMS } from './actions';
import { message } from 'antd';

export function* getMyStreams(action) {
	try {
		const { user } = action.payload;
		const response = yield call(() => Api.stream().getStreams({ user }));
		if (response.data && response.data.body && response.data.body.error) {
			message.warn('No user found');
		} else if (response.status === 200) {
			yield put({
				type: SAVE_MY_STREAMS,
				payload: response.data,
			});
		} else {
			message.warn('Bad login');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}

export function* playStream(action) {
	try {
		const { id, streamUrl, user } = action.payload;
		const response = yield call(() => Api.stream().playStream({ id, streamUrl, user }));
		if (response.data && response.data.body && response.data.body.error) {
			message.warn('No user found');
		} else if (response.status === 200) {
			console.info('playing stream');
		} else {
			message.warn('Bad login');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}

export function* stopStream(action) {
	try {
		const { user } = action.payload;
		const response = yield call(() => Api.stream().stopStream({ user }));
		if (response.data && response.data.body && response.data.body.error) {
			message.warn('No user found');
		} else if (response.status === 200) {
			console.info('stream stopped');
		} else {
			message.warn('Bad login');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}

export function* saveStream(action) {
	try {
		const response = yield call(() => Api.stream().saveSteam({ ...action.payload }));
		if (response.data && response.data.body && response.data.body.error) {
			message.warn('No user found');
		} else if (response.status === 200) {
			console.info('stream saved');
		} else {
			message.warn('Bad login');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}
