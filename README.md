# AnyVision interview task

## Intallation steps

- npm run install-server
- npm run install-client
- npm run install-db // creates db on localhost machine, mongo should be installed otherwise create a db with 2 collections [users, streams] and edit the .env
- Start the app with npm start

**Note: Tested on latest versions of Firefox and Chrome**

## Client App

- 4 screens [login, register, home, grid]
- Redux & Saga with reselect for global state managment and side effects
- RTSP modal player
- Input validations
- Styled Components
- Error Boundary

## Node App

- Node & Express App
- MongoDB
- JWT token authentication
- RTSP streaming
