import { call, put } from 'redux-saga/effects';
import Api from '../Utilities/Api';
import { SET_USER } from './actions';
import { message } from 'antd';

export function* logIn(action) {
	try {
		const { email, password } = action.payload;
		const response = yield call(() => Api.app().login({ email, password }));
		if (response.data && response.data.body && response.data.body.error) {
			message.warn('No user found');
		} else if (response.status === 200) {
			yield put({
				type: SET_USER,
				payload: response.data,
			});
		} else {
			message.warn('Bad login');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}

export function* register(action) {
	try {
		const { email, password, name } = action.payload;
		const response = yield call(() => Api.app().register({ email, password, name }));
		if (response.status === 200) {
			yield put({
				type: SET_USER,
				payload: response.data,
			});
		} else {
			message.warn('Bad Register');
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		console.error(err);
	}
}
