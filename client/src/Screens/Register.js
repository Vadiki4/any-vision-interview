import React, { useEffect, useRef, useCallback } from 'react';
import { RegisterLayout } from '../Components/Styled/PreApp';
import { Button, Input, message, Icon } from 'antd';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { REGISTER } from '../Sagas/actions';

const initialState = {
	name: null,
	email: null,
	password: null,
};

const RegisterPage = () => {
	const passRef = useRef();
	const emailRef = useRef();
	const nameRef = useRef();
	const [userPayload, setUserPayload] = useState(initialState);
	const history = useHistory();
	const dispatch = useDispatch();

	const handleRegister = useCallback(() => {
		// validate form fields
		for (const item in userPayload) {
			if (!userPayload[item] || !userPayload[item].length) {
				item === 'name'
					? nameRef.current.focus()
					: item === 'email'
					? emailRef.current.focus()
					: passRef.current.focus();
				message.warn(`Please fill your ${item}`);
				return;
			}
		}

		dispatch({ type: REGISTER, payload: userPayload });
	}, [dispatch, userPayload]);

	useEffect(() => {
		const handleEnterPress = e => e.keyCode === 13 && handleRegister();

		document.addEventListener('keypress', handleEnterPress);

		return () => {
			document.removeEventListener('keypress', handleEnterPress);
		};
	}, [handleRegister]);

	const handleInputChange = ({ target }) => {
		const { name, value } = target;
		setUserPayload(old => {
			return {
				...old,
				[name]: value,
			};
		});
	};

	return (
		<RegisterLayout height={window.innerHeight}>
			<div className='content'>
				<div className='logo'>
					<Icon type='key' />
					<span>Register</span>
				</div>
				<div className='elements'>
					{Object.keys(userPayload).map((item, idx) => (
						<div className='element' key={idx}>
							<Input
								name={item}
								value={userPayload[item]}
								placeholder={item}
								type={item === 'name' ? 'text' : item}
								size='large'
								ref={item === 'name' ? nameRef : item === 'email' ? emailRef : passRef}
								maxLength={50}
								onChange={handleInputChange}
							/>
						</div>
					))}
				</div>
				<div className='options'>
					<Button size='large' onClick={() => history.push('/login')} type='ghost'>
						Back to Login
					</Button>
					<Button size='large' onClick={handleRegister} type='primary'>
						Register
					</Button>
				</div>
			</div>
		</RegisterLayout>
	);
};

export default RegisterPage;
