import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Home from './Screens/Home';
import { useSelector } from 'react-redux';
import Login from './Screens/Login';
import Register from './Screens/Register';
import Grid from './Screens/Grid';
import { getCurrentUser } from './Selectors';

function App() {
	const [isAuthenticated, setAuthenticated] = useState(false);
	const user = useSelector(getCurrentUser);

	useEffect(() => {
		// redirecting non logged in users back to login
		if (user && user.token) {
			setAuthenticated(true);
		} else {
			setAuthenticated(false);
		}
	}, [user]);

	return (
		<div className='App'>
			<Router>
				<Switch>
					<Route
						path='/login'
						render={() => (isAuthenticated ? <Redirect to='/home' /> : <Login user={user} />)}
					/>
					<Route
						path='/register'
						render={() => (isAuthenticated ? <Redirect to='/home' /> : <Register user={user} />)}
					/>
					<Route
						path='/home'
						render={() => (isAuthenticated ? <Home user={user} /> : <Redirect to='/login' />)}
					/>
					<Route
						path='/streams'
						render={props => (isAuthenticated ? <Grid user={user} {...props} /> : <Redirect to='/login' />)}
					/>
					{isAuthenticated ? <Redirect to='/home' /> : <Redirect to='/login' />}
				</Switch>
			</Router>
		</div>
	);
}

export default App;
