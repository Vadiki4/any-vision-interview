import React, { useState } from 'react';
import { HomeLayout } from '../Components/Styled/App';
import { Input, Button, message, Icon } from 'antd';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { SAVE_STREAM, LOG_OUT } from '../Sagas/actions';
import { getCurrentUser } from '../Selectors';

function Home() {
	const [streamUrl, setStreamUrl] = useState(null);
	const [streamName, setStreamName] = useState(null);
	const user = useSelector(getCurrentUser);
	const dispatch = useDispatch();
	const history = useHistory();

	const handleSubmit = () => {
		const error = isValid();
		if (error) {
			message.warn(error);
		} else {
			dispatch({ type: SAVE_STREAM, payload: { streamUrl, streamName, user } });
			history.push('/streams', { dontCheck: true });
		}
	};

	const isValid = () => {
		// validate form fields
		if (!streamUrl || streamUrl.length < 4) {
			return 'Please fill out stream url';
		} else if (!streamName || streamName.length < 2) {
			return 'Please fill out stream name';
		} else {
			return null;
		}
	};

	const goToStreams = () => {
		history.push('/streams');
	};

	const logOut = () => {
		dispatch({ type: LOG_OUT });
	};

	return (
		<HomeLayout height={window.innerHeight}>
			<div className='content'>
				<div className='title'>
					<h3>Welcome, {user.name}</h3>
					<Icon type='logout' className='exit-btn' onClick={logOut} />
				</div>
				<div className='url'>
					<label>
						Add New Stream
						<Input
							size='large'
							placeholder='Stream url'
							onChange={({ target }) => setStreamUrl(target.value)}
							value={streamUrl}
						/>
					</label>
					<label>
						Stream Name
						<Input
							size='large'
							placeholder='Optional'
							onChange={({ target }) => setStreamName(target.value)}
							value={streamName}
						/>
					</label>
				</div>
				<div className='options'>
					<Button size='large' type='primary' onClick={handleSubmit}>
						Add Stream
					</Button>
					<Button size='large' type='ghost' onClick={goToStreams}>
						Streams
						<Icon type='wifi' />
					</Button>
				</div>
			</div>
		</HomeLayout>
	);
}

export default Home;
