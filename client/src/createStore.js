import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './Reducers';
import { initSagas } from './Sagas/initSagas';
import { setState, loadState } from './localStorage';
import throttle from 'lodash/throttle';

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistedState = loadState();

const store = createStore(reducers, persistedState, composeEnhancers(applyMiddleware(sagaMiddleware)));

// set up persistent store with throttling
store.subscribe(
	throttle(() => {
		setState({
			app: store.getState().app,
			streams: store.getState().streams,
		});
	}),
	1000
);

initSagas(sagaMiddleware);

export default store;
