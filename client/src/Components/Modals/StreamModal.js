import React, { useEffect, useRef } from 'react';
import JSMpeg from '@cycjimmy/jsmpeg-player';

function StreamModal({ playerState, wsPort = 9999 }) {
	const playerRef = useRef();

	useEffect(() => {
		if (playerState === 'stop' && playerRef.current) {
			playerRef.current.stop();
		} else if (playerState === 'play' || !playerRef.current) {
			try {
				// create streaming player and connect to the api
				playerRef.current = new JSMpeg.VideoElement('.stream-wrapper', `ws://localhost:${wsPort}`, {
					canvas: '#stream',
					autoplay: true,
					loop: false,
					controls: false,
				});
				playerRef.current.play();
			} catch (err) {
				console.error(err);
			}
		}
	}, [playerState]);

	useEffect(() => {
		return () => {
			// close player connection upon dismounting
			playerRef.current.destroy();
		};
	});

	return (
		<div className='stream-wrapper'>
			<canvas id='stream'></canvas>
		</div>
	);
}

export default StreamModal;
