import React, { useState, useEffect } from 'react';
import { GridLayout } from '../Components/Styled/App';
import { Button, List, Card, Modal, Icon } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { getMyStreams, getCurrentUser, getWsPort } from '../Selectors';
import { useHistory, useLocation } from 'react-router-dom';
import StreamModal from '../Components/Modals/StreamModal';
import { PLAY_STREAM, STOP_STREAM, GET_MY_STREAMS } from '../Sagas/actions';

function Grid() {
	const [streamModalVisible, setStreamModalVisible] = useState(false);
	const [streamUrl, setStreamURL] = useState(null);
	const [playerState, setPlayerState] = useState('play');
	const streamsList = useSelector(getMyStreams);
	const user = useSelector(getCurrentUser);
	const wsPort = useSelector(getWsPort);
	const history = useHistory();
	const dispatch = useDispatch();
	const location = useLocation();

	useEffect(() => {
		if (!location.state || !location.state.dontCheck) dispatch({ type: GET_MY_STREAMS, payload: { user } });
	}, [dispatch, user, location.state]);

	const goToStreamAdd = () => {
		history.push('/home');
	};

	const handleStreamSelect = item => {
		setStreamModalVisible(true);
		setStreamURL(item);
		dispatch({ type: PLAY_STREAM, payload: { ...item, user } });
	};

	const handleModalOk = () => {};

	const handleModalCancel = () => {
		setStreamModalVisible(false);
		setPlayerState('play');
		dispatch({ type: STOP_STREAM, payload: { user } });
	};

	return (
		<GridLayout height={window.innerHeight}>
			<div className='content'>
				<div className='title'>
					<Button size='large' type='ghost' onClick={goToStreamAdd}>
						<Icon type='arrow-left' />
						Add Stream
					</Button>
					<h3>MY STREAMS</h3>
				</div>
				<List
					grid={{
						gutter: 16,
						xs: 1,
						sm: 1,
						md: 2,
						lg: 2,
						xl: 4,
						xxl: 3,
					}}
					dataSource={streamsList}
					renderItem={item => (
						<List.Item className='list-item' onClick={() => handleStreamSelect(item)}>
							<Icon type='fullscreen' />
							<Card title={item.streamName}>
								<span>{item.streamUrl}</span>
							</Card>
						</List.Item>
					)}
				/>
			</div>
			<Modal
				title='Basic Modal'
				visible={streamModalVisible}
				destroyOnClose={true}
				onOk={handleModalOk}
				okButtonProps={{ style: { display: 'none' } }}
				onCancel={handleModalCancel}
			>
				<StreamModal playerState={playerState} wsPort={wsPort} />
			</Modal>
		</GridLayout>
	);
}

export default Grid;
