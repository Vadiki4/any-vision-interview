const mongoose = require('mongoose'),
	streamScope = require('./streamScope'),
	appScope = require('./appScope'),
	actions = require('../db/actions');
mongoose.connect(process.env.CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

module.exports = {
	app: async action => {
		switch (action.type) {
			case actions.LOG_IN: {
				const result = await appScope.logIn(db, action.data);
				return result;
			}
			case actions.REGISTER: {
				const result = await appScope.register(db, action.data);
				return result;
			}
			case actions.RESTORE: {
				const result = await appScope.restore(db, action.data);
				return result;
			}
		}
	},
	stream: async action => {
		switch (action.type) {
			case actions.SAVE_STREAM: {
				const result = await streamScope.saveStream(db, action.data);
				return result;
			}
			case actions.PLAY_STREAM: {
				const result = await streamScope.playStream(db, action.data);
				return result;
			}
			case actions.GET_MY_STREAMS: {
				const result = await streamScope.getMyStreams(db, action.data);
				return result;
			}
		}
	},
};
