const db = require('../db');
const actions = require('../db/actions');

module.exports = app => {
	app.post('/app/login', async (req, res) => {
		const { email, password } = req.body;
		// retrieve data from database
		const result = await db.app({ type: actions.LOG_IN, data: { email, password } });
		const { token, ...body } = result;
		// return to client
		res.status(200).send({ token, body });
	});
	app.post('/app/register', async (req, res) => {
		const { email, password, name } = req.body;
		// retrieve data from database
		const result = await db.app({ type: actions.REGISTER, data: { email, password, name } });
		const { token, ...body } = result;
		// return to client
		res.status(200).send({ token, body });
	});
};
